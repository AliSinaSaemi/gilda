from django import forms

from .submodels.contact import (
    ContactFullName,
    ContactTraditional
)

from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _


class ContactForm(forms.ModelForm):
    class Meta:
        model = ContactFullName
        fields = ('full_name', 'email', 'message', 'phone_number')


class ContactTraditionalForm(forms.ModelForm):
    class Meta:
        model = ContactTraditional
        fields = ('first_name', 'last_name', 'email', 'message', 'phone_number')

    def clean_first_name(self):
        first_name = self.cleaned_data.get('first_name')

        except_values = ['<', '>', '/', '+']

        for e_v in except_values:

            if e_v in first_name:
                raise ValidationError(
                    _('شما نمی توانید کاراکتر های خاص در این فیلد استفاده کنید.'),
                    code='forbidden'
                )

        return first_name

    def clean_lase_name(self):
        lase_name = self.cleaned_data.get('lase_name')

        except_values = ['<', '>', '/', '+']

        for e_v in except_values:

            if e_v in lase_name:
                raise ValidationError(
                    _('شما نمی توانید کاراکتر های خاص در این فیلد استفاده کنید.'),
                    code='forbidden'
                )

        return lase_name

    def clean_message(self):
        message = self.cleaned_data.get('message')

        except_values = ['<', '>', '/', '+']

        for e_v in except_values:

            if e_v in message:
                raise ValidationError(
                    _('شما نمی توانید کاراکتر های خاص در این فیلد استفاده کنید.'),
                    code='forbidden'
                )

        return message

    def clean_phone_number(self):

        phone_number = self.cleaned_data.get('phone_number')

        except_values = ['<', '>', '/', '+']

        for e_v in except_values:

            if e_v in phone_number:
                raise ValidationError(
                    _('شما نمی توانید کاراکتر های خاص در این فیلد استفاده کنید.'),
                    code='forbidden'
                )

        return phone_number