from django.contrib import admin
from .models import Site, Service
from .submodels.service import Category
from .submodels.banner import Banner
from .submodels.contact import ContactTraditional
from .submodels.newsletters import Newsletters


# Register your models here.


@admin.register(Site)
class SiteAdmin(admin.ModelAdmin):
    pass


@admin.register(Banner)
class BannerAdmin(admin.ModelAdmin):
    pass


@admin.register(ContactTraditional)
class ContactAdmin(admin.ModelAdmin):
    pass


@admin.register(Service)
class ServiceAdmin(admin.ModelAdmin):
    pass


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    pass

@admin.register(Newsletters)
class NewslettersAdmin(admin.ModelAdmin):
    pass