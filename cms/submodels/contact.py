from django.db import models

from django.utils.translation import ugettext_lazy as _

from painless.utils.regex.patterns import PERSIAN_PHONE_NUMBER_PATTERN

from painless.utils.models.common import TimeStampModelMixin

from django.core.validators import (
    RegexValidator,
    MinLengthValidator,
    MaxLengthValidator,
    EmailValidator,
)


class ContactFullName(TimeStampModelMixin):
    full_name = models.CharField(_("Full Name"), max_length=200)
    email = models.EmailField(_("Email"), validators=[EmailValidator], max_length=254)
    message = models.TextField(_("Message"))

    phone_number = models.CharField(
        _("Phone Number"),
        max_length=13,
        validators=[
            RegexValidator(
                PERSIAN_PHONE_NUMBER_PATTERN,
                message='please enter like these patterns `+989xxxxxxxxx`, `09xxxxxxxxx`.'
            ),
            MinLengthValidator(11),
            MaxLengthValidator(13)
        ],
    )

    class Meta:
        verbose_name = _('Contact')
        verbose_name_plural = _('Contacts')

    def __str__(self):
        return self.full_name

    def save(self, *args, **kwargs):
        super(ContactFullName, self).save(*args, **kwargs)


class ContactTraditional(TimeStampModelMixin):
    first_name = models.CharField(_("First Name"), max_length=200)
    last_name = models.CharField(_("Last Name"), max_length=200)
    email = models.EmailField(_("Email"), validators=[EmailValidator], max_length=254)
    message = models.TextField(_("Message"))

    phone_number = models.CharField(
        _("Phone Number"),
        max_length=13,
        validators=[
            RegexValidator(
                PERSIAN_PHONE_NUMBER_PATTERN,
                message='please enter like these patterns `+989xxxxxxxxx`, `09xxxxxxxxx`.'
            ),
            MinLengthValidator(11),
            MaxLengthValidator(13)
        ],
    )

    class Meta:
        verbose_name = _('Contact')
        verbose_name_plural = _('Contacts')

    def __str__(self):
        return f'{self.first_name} {self.last_name}'

    def save(self, *args, **kwargs):
        super(ContactTraditional, self).save(*args, **kwargs)
