from painless.utils.models.common import VideoModelMixin
from django.db import models
from django.utils.translation import ugettext_lazy as _

from django.core.validators import (
    MinLengthValidator,
    MaxLengthValidator,
)


class Banner(VideoModelMixin):
    title = models.CharField(
        _("Title"),
        max_length=50,
        validators=[
            MaxLengthValidator(50, message="عنوان بنر نمی تواند بیش از 50 کاراکتر باشد"),
            MinLengthValidator(10, message="عنوان بنر نمی تواند کمتر از 10 کاراکتر باشد")
        ],
    )
    
    summary = models.CharField(
        _("Summary"),
        max_length=250,
        validators=[
            MaxLengthValidator(250, message="توضیحات بنر نمی تواند بیش از 250 کاراکتر باشد"),
            MinLengthValidator(30, message="توضیحات بنر نمی تواند کمتر از 50 کاراکتر باشد")
        ],
    )
    
    def __str__(self):
        return self.title
    
    class Meta:
        verbose_name = _("Banner")
        verbose_name_plural = _("Banners")