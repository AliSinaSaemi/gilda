from django.core.validators import EmailValidator
from django.db import models
from django.utils.translation import ugettext_lazy as _

from painless.utils.models.common import TimeStampModelMixin


class Newsletters(TimeStampModelMixin):
    email = models.EmailField(
        _("Email"),
        max_length=150,
        validators=[
            EmailValidator(),
        ],
    )

    def __str__(self):
        return self.email
