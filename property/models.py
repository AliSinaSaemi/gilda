import secrets

from django.db import models
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _

from django.urls import reverse

from django.core.validators import (
    FileExtensionValidator,
    RegexValidator, MaxLengthValidator, MinLengthValidator
)

from painless.utils.upload.path import date_directory_path

from painless.utils.models.common import (
    TimeStampModelMixin,
    ImageModelMixin,
    TitleSlugLinkModelMixin,
    VideoModelMixin
)


class Property(TitleSlugLinkModelMixin, ImageModelMixin, TimeStampModelMixin):
    STATUS = (
        ('r', _('اجاره')),
        ('b', _('فروش'))
    )

    KIND = (
        ('a', _('آپارتمان')),
        ('h', _('خانه')),
        ('g', _('زمین'))
    )

    poster_picture = models.ImageField(
        _("Poster Picture"),
        upload_to=date_directory_path,
        height_field='poster_height_field',
        width_field='poster_width_field',
        max_length=110,
        blank=True,
        null=True,
        validators=[FileExtensionValidator(allowed_extensions=['JPG', 'JPEG', 'PNG', 'jpg', 'jpeg', 'png'])]
    )

    poster_picture_alternate_text = models.CharField(
        _("Poster Picture Alternate Text"),
        max_length=110,
        validators=[
            MaxLengthValidator(150),
            MinLengthValidator(3)
        ],
        blank=True,
        null=True
    )
    poster_width_field = models.PositiveSmallIntegerField(_("Width Field"), blank=True, null=True, editable=False)
    poster_height_field = models.PositiveSmallIntegerField(_("Height Field"), blank=True, null=True, editable=False)

    video = models.FileField(
        _("Video"),
        upload_to=date_directory_path,
        max_length=110,
        validators=[
            FileExtensionValidator(allowed_extensions=['mp4', ])
        ],
        blank=True,
        null=True
    )

    video_alternate_text = models.CharField(
        _("Video Alternate Text"),
        max_length=110,
        validators=[
            MaxLengthValidator(150),
            MinLengthValidator(3)
        ],
        blank=True,
        null=True
    )

    sku = models.CharField(_("SKU"), editable=False, max_length=7)

    price = models.CharField(
        _("Price"),
        max_length=16,
        validators=[
            RegexValidator(r'\d+', message=_('price must be digit.')),
        ],
    )
    is_featured = models.BooleanField(_("Is Featured"), default=False)
    is_popular = models.BooleanField(_("Is Popular"), default=False)

    status = models.CharField(_("Status"), choices=STATUS, max_length=1)
    kind = models.CharField(_("Kind"), choices=KIND, max_length=1)
    area = models.CharField(
        _("Area"),
        max_length=12,
        validators=[RegexValidator(r'\d+x\d+',
                                   message=_("Please enter area in this format `(100x200)`. TIP: (width, height) "))]
    )

    description = models.TextField(_("Description"))

    category = models.ForeignKey('Category', related_name='properties', on_delete=models.PROTECT)

    def save(self, *args, **kwargs):
        self.sku = secrets.token_hex(3).upper()
        self.slug = slugify(self.title, allow_unicode=True)
        super(Property, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('pages:property', kwargs={'slug': self.slug})

    def get_price(self):
        return "{:,.0f}".format(float(self.price))

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _('Property')
        verbose_name_plural = _('Properties')


class Category(TitleSlugLinkModelMixin, TimeStampModelMixin):
    class Meta:
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title, allow_unicode=True)
        super(Category, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('pages:property_category', kwargs={'slug': self.slug, 'page': 1})

    def __str__(self):
        return self.title


class Tag(TitleSlugLinkModelMixin, TimeStampModelMixin):
    item = models.ManyToManyField("Property", db_column='property', related_name='tag', verbose_name=_("Tag Property"))

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title, allow_unicode=True)
        super(Tag, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('pages:property_tag', kwargs={'slug': self.slug, 'page': 1})

    def __str__(self):
        return self.title


class Feature(TitleSlugLinkModelMixin, TimeStampModelMixin):
    item = models.ForeignKey(
        Property,
        db_column='property',
        verbose_name=_("Property"),
        related_name='features',
        on_delete=models.CASCADE
    )

    icon = models.ForeignKey(
        'Icon',
        db_column='icon',
        verbose_name=_("Icon"),
        related_name='features',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title, allow_unicode=True)
        super(Feature, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('property_feature', kwargs={'slug': self.slug})

    def __str__(self):
        return self.title


class Gallery(TimeStampModelMixin, ImageModelMixin):
    item = models.ForeignKey(Property, db_column='property', verbose_name=_("Feature"), related_name='pictures',
                             on_delete=models.CASCADE)

    def __str__(self):
        return self.picture_alternate_text

    def get_absolute_url(self):
        return reverse('property_gallery', kwargs={'slug': self.slug})

    class Meta:
        verbose_name = _('Gallery')
        verbose_name_plural = _('Galleries')


class Amenities(TimeStampModelMixin):
    key = models.CharField(_("Key"), max_length=150)
    value = models.BooleanField(_("Value"))
    item = models.ForeignKey(Property, db_column='property', verbose_name=_("Amenities"), related_name='ameneties',
                             on_delete=models.CASCADE)

    def get_absolute_url(self):
        return reverse('property_aminity', kwargs={'slug': self.slug})

    def __str__(self):
        return self.key


class Detail(TimeStampModelMixin):
    key = models.CharField(_("Key"), max_length=150)
    value = models.CharField(_("Key"), max_length=150)
    item = models.ForeignKey(Property, db_column='property', verbose_name=_("Detail"), related_name='details',
                             on_delete=models.CASCADE)

    def get_absolute_url(self):
        return reverse('property_detail', kwargs={'slug': self.slug})

    def __str__(self):
        return self.key


class Icon(models.Model):
    name = models.CharField(
        max_length=30,
        verbose_name=_("Name"),
    )

    code = models.CharField(
        _("Code"),
        max_length=30,
    )

    def __str__(self):
        return self.name
