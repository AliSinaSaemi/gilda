from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render
from django.utils.translation import ugettext_lazy as _
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy, reverse

from django.views.generic import (
    TemplateView,
    ListView,
    CreateView,
    DetailView,
    View
)

from property.models import Property, Tag, Feature

from cms.models import Site
from cms.submodels.service import Service
from cms.submodels.banner import Banner
from cms.submodels.contact import ContactTraditional
from cms.form import ContactTraditionalForm
from property.models import Category
from cms.submodels.newsletters import Newsletters


class HomeView(TemplateView):
    template_name = "pages/index.html"
    page_name = _("خانه")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['site'] = Site.objects.first()
        context['banner'] = Banner.objects.first()
        context['properties'] = Property.objects.filter(is_featured=True).order_by('-created')[:3]
        context['popularـproperties'] = Property.objects.filter(is_popular=True).order_by('-created')[:4]
        context['services'] = Service.objects.all().order_by('-created')[:4]
        context['categories'] = Category.objects.all()
        return context


class ContactCreateView(SuccessMessageMixin, ContactTraditionalForm, CreateView):
    model = ContactTraditional
    form = ContactTraditionalForm
    template_name = "pages/contact-us.html"
    success_url = reverse_lazy('pages:contact')
    page_name = _("تماس باما")
    success_message = _("اطلاعات شما با موفقیت ذخیره شد. به زودی با شما تماس گرفته خواهد شد.")

    def get_context_data(self, **kwargs):
        site = Site.objects.first()
        context = super().get_context_data(**kwargs)
        context['site'] = site
        context['categories'] = Category.objects.all()
        return context


class AboutView(TemplateView):
    template_name = "pages/about-us.html"
    page_name = _("درباره ما")

    def get_context_data(self, **kwargs):
        site = Site.objects.first()
        context = super().get_context_data(**kwargs)
        context['site'] = site
        context['services'] = Service.objects.all().order_by('-created')[:4]
        context['categories'] = Category.objects.all()
        return context


class PropertyListView(ListView):
    page_name = _("املاک")
    queryset = Property.objects.all().order_by('-created')
    paginate_by = 6
    template_name = "pages/properties.html"
    context_object_name = 'properties'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['site'] = Site.objects.first()
        context['recent_properties'] = Property.objects.all().order_by('-created')[:3]
        context['recent_tags'] = Tag.objects.all().order_by('-created')[:10]
        context['categories'] = Category.objects.all()
        return context


class PropertyDetailView(DetailView):
    model = Property
    template_name = 'pages/property.html'
    context_object_name = 'property'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['site'] = Site.objects.first()
        context['recent_properties'] = Property.objects.all().order_by('-created')[:3]
        context['featured_properties'] = Property.objects.all().order_by('is_featured', '-created')[:3]
        context['properties'] = Property.objects.filter(is_featured=True).order_by('-created')[:3]
        context['categories'] = Category.objects.all()
        return context


class PropertyTagDetailView(View):
    template_name = 'pages/property-tag.html'

    paginated_by = 6

    page_name = _('برچسب')

    def get_context_data(self, **kwargs):

        context = dict()

        context['site'] = Site.objects.first()
        context['recent_properties'] = Property.objects.all().order_by('-created')[:3]
        context['recent_tags'] = Tag.objects.all().order_by('-created')[:10]
        context['categories'] = Category.objects.all()
        return context

    def get(self, request, slug, page, *args, **kwargs):

        tag = Tag.objects.get(slug=slug)

        properties = tag.item.all().order_by("-created")

        paginator = Paginator(properties, self.paginated_by)

        try:

            properties = paginator.get_page(page)

        except PageNotAnInteger:

            properties = paginator.get_page(1)

        except EmptyPage:

            properties = paginator.page(paginator.num_pages)

        context = self.get_context_data()

        context['properties'] = properties
        context['tag'] = tag

        return render(
            request,
            self.template_name,
            context
        )


class ListCategoryProperties(View):
    template_name = 'pages/property-category.html'

    paginated_by = 6

    page_name = _('دسته‌بندی')

    def get_context_data(self, **kwargs):

        context = dict()

        context['site'] = Site.objects.first()
        context['recent_properties'] = Property.objects.all().order_by('-created')[:3]
        context['recent_tags'] = Tag.objects.all().order_by('-created')[:10]
        context['categories'] = Category.objects.all()
        return context

    def get(self, request, slug, page, *args, **kwargs):
        category = Category.objects.get(slug=slug)

        properties = category.properties.all().order_by("-created")

        paginator = Paginator(properties, self.paginated_by)

        try:

            properties = paginator.get_page(page)

        except PageNotAnInteger:

            properties = paginator.get_page(1)

        except EmptyPage:

            properties = paginator.page(paginator.num_pages)

        context = self.get_context_data()

        context['properties'] = properties
        context['category'] = category

        return render(
            request,
            self.template_name,
            context
        )


class NewsletterPartialView(CreateView):
    success_url = 'pages:home'
    model = Newsletters
    fields = ['email', ]
    template_name = 'partials/footer.html'
    success_message = _("ایمیل شما با موفقیت ثبت شد.")

    def get_success_url(self, *args):
        return reverse('pages:home')

    def get_context_data(self, **kwargs):
        kwargs['site'] = Site.objects.first()
        return super(NewsletterPartialView, self).get_context_data(**kwargs)