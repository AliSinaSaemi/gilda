from django.contrib import admin

from django.views.decorators.cache import cache_page

from django.urls import (
    path,
    re_path,
    include
)

from . import views

app_name = 'pages'

urlpatterns = [

    re_path(
        r'^$',
        views.HomeView.as_view(),
        name='home'),

    re_path(
        r'^contact/$',
        views.ContactCreateView.as_view(),
        name='contact'),

    re_path(
        r'^about/$',
        views.AboutView.as_view(),
        name='about'),

    re_path(
        r'^properties/$',
        views.PropertyListView.as_view(),
        name='properties'),

    re_path(
        r'^properties/page/(?P<page>[\d]+)/$',
        views.PropertyListView.as_view(),
        name='properties'
    ),

    re_path(
        r'^property/(?P<slug>[-\w]+)/$',
        views.PropertyDetailView.as_view(),
        name='property'),

    re_path(
        r'^property/tag/(?P<slug>[-\w]+)/page/(?P<page>[\d]+)/$',
        views.PropertyTagDetailView.as_view(),
        name='property_tag'),

    re_path(
        r'^property/category/(?P<slug>[-\w]+)/page/(?P<page>[\d]+)/$',
        views.ListCategoryProperties.as_view(),
        name='property_category'
    ),

    re_path(
        r'^footer/$',
        views.NewsletterPartialView.as_view(),
        name='footer'),

]
