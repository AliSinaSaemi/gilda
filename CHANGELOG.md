# Changelog

## [Unreleased]

## [0.0.0] - 2020-09-18
### Added
- Modular Settings with python-decouple module
- gitignore standard for django
- basic requirements.txt file

## [0.1.0] - 2020-09-18
### Added
- property application

## [released]
## [1.0.0] - 2020-00-00
### Added
