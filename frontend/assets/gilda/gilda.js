$(document).ready(function () {
  $(window).scroll(function () {
    if ($(this).scrollTop() > 100) {
      $("#scroll").fadeIn();
    } else {
      $("#scroll").fadeOut();
    }
  });
  $("#scroll").click(function () {
    $("html, body").animate({ scrollTop: 0 }, 600);
    return false;
  });
});

$("#lightSlider").lightSlider({
  gallery: true,
  item: 1,
  loop: true,
  slideMargin: 0,
  thumbItem: 6,
  controls: true,
  prevHtml: "<i class='fas fa-angle-left'></i>",
  nextHtml: "<i class='fas fa-angle-right'></i>",
});

function openNav() {
  document.getElementById("mySidenav").style.width = "100%";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
}

$(document).ready(function () {
  $(".owl-two").owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    autoplay: false,

    responsive: {
      0: {
        items: 1,
      },
      576: {
        items: 2,
      },
      768: {
        items: 2,
      },
      992: {
        items: 3,
      },
    },
  });
});
$(document).ready(function () {
  $(".owl-three").owlCarousel({
    loop: true,
    margin: 30,
    nav: false,
    autoplay: false,
    navText: [
      "<i class='fas fa-angle-left'></i>",
      "<i class='fas fa-angle-right'></i>",
    ],
    responsive: {
      0: {
        items: 1,
      },
      576: {
        items: 2,
      },
      768: {
        items: 2,
      },
      992: {
        items: 3,
      },
      1200: {
        items: 5,
      },
    },
  });
});

$(document).ready(function () {
  $(".owl-four").owlCarousel({
    loop: true,
    margin: 0,
    nav: true,
    autoplay: false,
    autoplayTimeout: 6000,
    autoplaySpeed: 2000,
    autoplayHoverPause: true,
    navText: [
      "<i class='fas fa-angle-left'></i>",
      "<i class='fas fa-angle-right'></i>",
    ],
    responsive: {
      0: {
        items: 1,
      },
      1200: {
        items: 1,
      },
    },
  });
});

$(document).ready(function () {
  $(".owl-one").owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    autoplay: false,
    navText: [
      "<i class='fas fa-angle-left'></i>",
      "<i class='fas fa-angle-right'></i>",
    ],
    responsive: {
      0: {
        items: 1,
      },
      600: {
        items: 1,
      },
      1000: {
        items: 1,
      },
    },
  });
});

(function ($) {
  $(document).ready(function () {
    $("#cssmenu li.has-sub>a").on("click", function () {
      $(this).removeAttr("href");
      var element = $(this).parent("li");
      if (element.hasClass("open")) {
        element.removeClass("open");
        element.find("li").removeClass("open");
        element.find("ul").slideUp();
      } else {
        element.addClass("open");
        element.children("ul").slideDown();
        element.siblings("li").children("ul").slideUp();
        element.siblings("li").removeClass("open");
        element.siblings("li").find("li").removeClass("open");
        element.siblings("li").find("ul").slideUp();
      }
    });

    (function getColor() {
      var r, g, b;
      var textColor = $("#cssmenu").css("color");
      textColor = textColor.slice(4);
      r = textColor.slice(0, textColor.indexOf(","));
      textColor = textColor.slice(textColor.indexOf(" ") + 1);
      g = textColor.slice(0, textColor.indexOf(","));
      textColor = textColor.slice(textColor.indexOf(" ") + 1);
      b = textColor.slice(0, textColor.indexOf(")"));
      var l = rgbToHsl(r, g, b);
      if (l > 0.7) {
        $("#cssmenu>ul>li>a").css(
          "text-shadow",
          "0 1px 1px rgba(0, 0, 0, .35)"
        );
        $("#cssmenu>ul>li>a>span").css("border-color", "rgba(0, 0, 0, .35)");
      } else {
        $("#cssmenu>ul>li>a").css(
          "text-shadow",
          "0 1px 0 rgba(255, 255, 255, .35)"
        );
        $("#cssmenu>ul>li>a>span").css(
          "border-color",
          "rgba(255, 255, 255, .35)"
        );
      }
    })();

    function rgbToHsl(r, g, b) {
      (r /= 255), (g /= 255), (b /= 255);
      var max = Math.max(r, g, b),
        min = Math.min(r, g, b);
      var h,
        s,
        l = (max + min) / 2;

      if (max == min) {
        h = s = 0;
      } else {
        var d = max - min;
        s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
        switch (max) {
          case r:
            h = (g - b) / d + (g < b ? 6 : 0);
            break;
          case g:
            h = (b - r) / d + 2;
            break;
          case b:
            h = (r - g) / d + 4;
            break;
        }
        h /= 6;
      }
      return l;
    }
  });
})(jQuery);
